#!/bin/sh 
infile1=$1; 
echo $infile
# select years to calculate the baseline monthly means 
cdo -f nc4c -z zip_6 selyear,2001/2015 $infile1 tmp1.nc

# calculate the monthly means 
cdo -f nc4c -z zip_6 ymonmean tmp1.nc tmp2.nc

# subtract the monthly means from the full file
cdo ymonsub $infile1 tmp2.nc tmp3.nc

# Linear detrending 
cdo -f nc4c -z zip_6 detrend tmp3.nc anoms/$infile1

