library(lubridate); library(viridis); library(mgcv);
library(rstanarm); library(tidyverse); library(loo); 
options(mc.cores = parallel::detectCores()-1)
library(RcppRoll); library(visreg)
# --- Data prep ----------------------------------------------------------------------------------
clim <- read_csv("data/clim/terraClim_chirps_merged_bySite.csv", guess_max = 1e5) %>% 
  group_by(site) %>% arrange(date) %>% 
  mutate(mdef.tc = roll_maxr(def.tc, n=12, fill=NA)) %>% select(-u_precip)
tmp <- clim %>% filter(year>=1986 & year <= 2016) %>% 
  group_by(site,month) %>% 
  summarize(u_def.tc = mean(def.tc, na.rm=T), 
            u_mdef.tc = mean(mdef.tc, na.rm=T), 
            u_precip.tc = mean(precip.tc, na.rm=T), 
            u_pdsi.tc = mean(pdsi.tc, na.rm=T), 
            u_precip = mean(precip, na.rm=T))
clim <- left_join(clim, tmp, by=c("site","month"))
clim <- clim %>% mutate(def_anom.tc = def.tc-u_def.tc, 
                        mdef_anom.tc = mdef.tc - u_mdef.tc, 
                        precip_anom.tc = precip.tc - u_precip.tc, 
                        pdsi_anom.tc = pdsi.tc - u_pdsi.tc, 
                        precip_anom = precip - u_precip) %>% 
  mutate(wet_season = ifelse(u_def.tc <= 1, 1, 0))

ceres <- read_csv("data/clim/CERES_sfc_sw_GEM_sites_2018-08-06.csv") %>% rename(sw.ceres =sw)
tmp <- ceres %>% mutate(month=month(date), year=year(date)) %>% 
  group_by(site, month) %>% summarize(u_sw.ceres = mean(sw.ceres,na.rm=T))
ceres <- left_join(ceres %>% mutate(month=month(date)), tmp, by=c("site","month"))
ceres <- ceres %>% mutate(sw_anom.ceres = sw.ceres-u_sw.ceres) 
clim <- left_join(clim,ceres, by=c("site","date","month"))

tmp <- clim %>% filter(year>=1986 & year <= 2016) %>% 
  group_by(site, month) %>% 
  summarize(cwd_sigma = sd(cwd_et, na.rm=T), 
            mcwd_sigma = sd(mcwd, na.rm=T), 
            wdt_sigma = sd(wdt, na.rm=T), 
            Tmean_sigma = sd(Tmean, na.rm=T), 
            Tmean_sigma.tc = sd(Tmean.tc, na.rm=T), 
            VPDmean_sigma = sd(VPDmean, na.rm=T),
            VPDmean_sigma.tc = sd(VPDmean.tc, na.rm=T),
            vap_sigma.tc = sd(vap.tc, na.rm=T),
            sw_sigma.tc = sd(sw.tc, na.rm=T), 
            sw_sigma.ceres = sd(sw.ceres, na.rm=T),
            cloud_fraction_sigma = sd(cloud_fraction, na.rm=T), 
            def_sigma.tc = sd(def.tc, na.rm=T), 
            mdef_sigma.tc = sd(mdef.tc, na.rm=T), 
            precip_sigma.tc = sd(precip.tc, na.rm=T), 
            pdsi_sigma.tc = sd(pdsi.tc, na.rm=T), 
            precip_sigma = sd(precip, na.rm=T))
clim <- left_join(clim, tmp, by=c('site','month'))

clim <- clim %>% mutate(u_cloud_fraction=cloud_fraction-cloud_fraction_anom) %>% 
  mutate(u_p_et = u_precip/et) %>%
  mutate(p_et = precip/et) %>%
  mutate(u_season = ifelse(u_precip>et, ("wet"), ("dry")),
         season = ifelse(precip>pet, ("wet"), ("dry")), 
         p_et100 = ifelse(precip>100,T,F), 
         p_et_bin = ifelse(precip>et, T, F), 
         u_p_et_bin = ifelse(u_precip >= et, T, F)) %>% 
  mutate(cwd_rat = cwd_anom/(u_cwd)) %>% 
  mutate(cwd_anom_sigma = cwd_anom/(cwd_sigma+1), 
         mcwd_anom_sigma = mcwd_anom/(mcwd_sigma+1),
         wdt_anom_sigma = wdt_anom/wdt_sigma, 
         Tmean_anom_sigma = Tmean_anom/Tmean_sigma, 
         Tmean_anom_sigma.tc = Tmean_anom.tc/Tmean_sigma.tc, 
         VPDmean_anom_sigma = VPDmean_anom/VPDmean_sigma, 
         VPDmean_anom_sigma.tc = VPDmean_anom.tc/VPDmean_sigma.tc,
         vap_anom_sigma.tc = vap_anom.tc/vap_sigma.tc, 
         sw_anom_sigma.tc = sw_anom.tc/sw_sigma.tc, 
         sw_anom_sigma.ceres = sw_anom.ceres/sw_sigma.ceres,
         cloud_fraction_anom_sigma = cloud_fraction_anom/cloud_fraction_sigma, 
         def_anom_sigma.tc = def_anom.tc/(def_sigma.tc+1), 
         mdef_anom_sigma.tc = mdef_anom.tc/(mdef_sigma.tc+1), 
         precip_anom_sigma.tc = precip_anom.tc/precip_sigma.tc, 
         pdsi_anom_sigma.tc = pdsi_anom.tc/pdsi_sigma.tc, 
         precip_anom_sigma = precip_anom/precip_sigma)
clim <- clim %>% group_by(site) %>% arrange(date) %>% 
  mutate(cwd_anom_sigma_3mo = roll_meanr(cwd_anom_sigma, n = 3), 
         sw_anom_sigma_3mo.ceres = roll_meanr(sw_anom_sigma.ceres, n=3), 
         sw_anom_sigma_3mo.tc = roll_meanr(sw_anom_sigma.tc, n=3), 
         Tmean_anom_sigma_3mo = roll_meanr(Tmean_anom_sigma, n=3),
         Tmean_anom_sigma_3mo.tc = roll_meanr(Tmean_anom_sigma.tc, n=3),
         VPDmean_anom_sigma_3mo = roll_meanr(VPDmean_anom_sigma, n=3),
         VPDmean_anom_sigma_3mo.tc = roll_meanr(VPDmean_anom_sigma.tc, n=3), 
         vap_anom_sigma_3mo.tc = roll_meanr(vap_anom_sigma.tc,n=3),
         cloud_fraction_anom_sigma_3mo = roll_meanr(cloud_fraction_anom_sigma, n=3),
         precip_anom_sigma_3mo.tc = roll_meanr(precip_anom_sigma.tc, n=3), 
         pdsi_anom_sigma_3mo.tc = roll_meanr(pdsi_anom_sigma.tc, n=3),
         precip_anom_sigma_3mo = roll_meanr(precip_anom_sigma, n=3)) %>% 
  ungroup()
clim <- clim %>%   mutate(dry_anom_sigma.tc = ifelse(def_anom_sigma.tc <= 0, -def_anom_sigma.tc, 0), 
                          wet_anom_sigma.tc = ifelse(def_anom_sigma.tc > 0, def_anom_sigma.tc, 0), 
                          dark_anom_sigma.tc = ifelse(sw_anom_sigma_3mo.tc <= 0, sw_anom_sigma_3mo.tc, 0), 
                          bright_anom_sigma.tc = ifelse(sw_anom_sigma_3mo.tc > 0, sw_anom_sigma_3mo.tc, 0))
stem <- read_csv("data/gem_stem_npp/DataTable_NPPliveWood_20180807.csv")
stem <- stem %>% rename(stem=mu3_hybrid) %>% filter(is.na(stem)==F)
stem <- stem %>% filter(!site %in% "STN") %>% 
  filter(!plot_code %in% c("STD-11","STJ-04"))
stem <- stem %>%  
  filter(site != "CAX") %>%
  filter(!(plot_code %in% c("STB-12","STJ-01","STQ-08",
                            "STJ-01","STJ-05","STL-09","STL-10") &
             (date >= as.POSIXct("2016-01-15",tz="UTC")))) 

stem_tc <- left_join(stem, clim, by=c("site","year","month","date")) %>% 
  filter(is.na(stem)==F) %>% filter(stem > -0.125 & stem < 0.4) %>% 
  mutate(dry_anom_sigma.tc = ifelse(def_anom_sigma.tc <= 0, -def_anom_sigma.tc, 0), 
         wet_anom_sigma.tc = ifelse(def_anom_sigma.tc > 0, def_anom_sigma.tc, 0), 
         dark_anom_sigma.tc = ifelse(sw_anom_sigma_3mo.tc <= 0, sw_anom_sigma_3mo.tc, 0), 
         bright_anom_sigma.tc = ifelse(sw_anom_sigma_3mo.tc > 0, sw_anom_sigma_3mo.tc, 0))  
stem_dry <- stem_tc %>% filter(S > 0.05)
stem_wet <- stem_tc %>% filter(S < 0.05)

# Best dry site fit -------------------------------------------------------
s_d4_plotting <- stan_gamm4(stem ~ 
                     u_VPDmean.tc_sc+
                     u_Tmean.tc_sc+
                     u_sw.tc_sc+  
                     dry_anom_sigma.tc+
                     wet_anom_sigma.tc+
                     s(sw_anom_sigma_3mo.tc, k=5), 
                   random = ~ (1|plot_code), 
                   family=Gamma(link='log'), 
                   algorithm='sampling',
                   chains=3, iter=2000,
                   prior=normal(0,1), QR=T,
                   data=stem_dry %>% 
                     mutate(site=as.factor(site), 
                            plot_code=as.factor(plot_code),
                            u_VPDmean.tc_sc = scale(u_VPDmean.tc)[,1],
                            u_Tmean.tc_sc = scale(u_Tmean.tc)[,1],
                            u_sw.tc_sc = scale(u_sw.tc)[,1],
                            stem=stem+0.2))
s_d4_preds <- stan_gamm4(stem ~ 
                              u_VPDmean.tc+
                              u_Tmean.tc+
                              u_sw.tc+  
                              dry_anom_sigma.tc+
                              wet_anom_sigma.tc+
                              s(sw_anom_sigma_3mo.tc, k=5), 
                            random = ~ (1|plot_code), 
                            family=Gamma(link='log'), 
                            algorithm='sampling',
                            chains=3, iter=2000,
                            prior=normal(0,1), QR=T, adapt_delta = 0.99,
                            data=stem_dry %>% 
                              mutate(site=as.factor(site), 
                                     plot_code=as.factor(plot_code),
                                     u_VPDmean.tc_sc = scale(u_VPDmean.tc)[,1],
                                     u_Tmean.tc_sc = scale(u_Tmean.tc)[,1],
                                     u_sw.tc_sc = scale(u_sw.tc)[,1],
                                     stem=stem+0.2))
plot_nonlinear(s_d4_preds)
# Best wet site fit -------------------------------------------------------
s_wcombo <- stan_glmer(stem ~ 
                         VPDmean_anom_sigma_3mo.tc+
                         def_anom_sigma.tc+
                         sw_anom_sigma_3mo.ceres+
                         (1|plot_code)+(1|site), 
                       family=Gamma(link='log'), 
                       algorithm='sampling',
                       chains=3, iter=2000, 
                       adapt_delta = 0.99,
                       prior=normal(0,1), QR=T, data=stem_wet %>% mutate(stem=stem+0.2))


# Plotting libraries ------------------------------------------------------
library(gridExtra)
library(tidybayes)

# Coefficient plot --------------------------------------------------------
# seasonal forest coef plot
plot(s_d4_plotting, pars=names(s_d4_plotting$coefficients)[2:6])
p1 <- plot(s_d4_plotting, pars=names(s_d4_plotting$coefficients)[2:6]); p1
# p1$plot_env$data$parameter
p1a <- p1+
  scale_y_discrete(
    # limits=       rev(c("u_VPDmean.tc_sc",
    #                                    "u_Tmean.tc_sc",
    #                                    "u_sw.tc_sc",
    #                                    "dry_anom_sigma.tc",
    #                                    "wet_anom_sigma.tc")),
                          labels=(c(bquote(paste('VPDmean'[mu],)),
                                       bquote(paste('Tmean'[mu],)),
                                       bquote(paste('SWmean'[mu],)),
                                       "Dry anom.",
                                       "Wet anom.")))+
  labs(title=bquote(paste("Seasonal Forest Model")), x="Standardized Effect Size")+
  theme(axis.text=element_text(size=14, face='bold'), 
        axis.title = element_text(size=16, face='bold'),
        plot.title = element_text(size=22,face='bold'))
p1a; 

p1b <- 
  tibble(u_VPDmean.tc=mean(stem_dry$u_VPDmean.tc), 
                       u_Tmean.tc=mean(stem_dry$u_Tmean.tc),
                       u_sw.tc=mean(stem_dry$u_sw.tc),
                       # sw_anom_sigma_3mo.tc = mean(stem_dry$sw_anom_sigma_3mo.tc, na.rm=T)),
                       sw_anom_sigma_3mo.tc = 0,
                       dry_anom_sigma.tc = seq(0,4,length.out = 200),
                       wet_anom_sigma.tc=0, 
                       water_deficit_anom = seq(0,4,length.out=200),
                       condition="Drought") %>% 
  bind_rows(., 
            tibble(u_VPDmean.tc=mean(stem_dry$u_VPDmean.tc), 
                   u_Tmean.tc=mean(stem_dry$u_Tmean.tc),
                   u_sw.tc=mean(stem_dry$u_sw.tc),
                   sw_anom_sigma_3mo.tc = 0,
                   dry_anom_sigma.tc = 0,
                   wet_anom_sigma.tc=seq(0,2.25,length.out = 200),
                   water_deficit_anom = seq(0,-2.25,length.out=200),
                   condition="Wet") 
  ) %>% 
  add_predicted_draws(model = s_d4_preds, newdata = ., n = 3000, re_formula = NA) %>% 
  ggplot(data=., aes(water_deficit_anom, NA))+
  geom_vline(xintercept = 0,lty=1,col='darkgrey')+
  stat_lineribbon(aes(y = .prediction-0.2), .width = c(.99, .75, .50), alpha = 0.45, col='black') +
  geom_jitter(data=stem_dry, aes(def_anom_sigma.tc, stem),
              alpha=0.45,cex=2, col="#352702",width = 0.4)+
  scale_fill_viridis_d("Probability",option = "B", direction = -1, begin = 0.1, end = 0.9) + 
  scale_color_viridis_c()+
  scale_x_continuous(expand = c(0,0))+
  labs(x=expression(paste("Water Deficit Anomaly [",sigma,"]")), 
     y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1'))) +
  annotate("text",label="Wetter",x=-1.5,y=-0.1,size=6, fontface='bold')+
  annotate("text",label="Drier",x=3.5,y=-0.1, size=6, fontface='bold')+ theme(legend.position = 'none')
p1b

p1c <- 
  tibble(u_VPDmean.tc=mean(stem_dry$u_VPDmean.tc), 
         u_Tmean.tc=mean(stem_dry$u_Tmean.tc),
         u_sw.tc=mean(stem_dry$u_sw.tc),
         sw_anom_sigma_3mo.tc = seq(-2.6,2.6,length.out=500),
         dry_anom_sigma.tc=0,
         wet_anom_sigma.tc=0) %>% 
  add_predicted_draws(model = s_d4_preds, newdata = ., n = 3000, re_formula = NA) %>% 
  ggplot(data=., aes(sw_anom_sigma_3mo.tc, NA))+
  geom_vline(xintercept = 0,lty=1,col='darkgrey')+
  stat_lineribbon(aes(y = .prediction-0.2), .width = c(.99, .75, .50), alpha = 0.4, col='black') +
  geom_jitter(data=stem_dry, aes(sw_anom_sigma_3mo.tc, stem),
              alpha=0.4,cex=2, col="#352702",width = 0.4)+
  scale_fill_viridis_d("Probability",option = "D", direction = -1, begin = 0.1, end = 0.95) + 
  scale_color_viridis_c()+
  scale_x_continuous(expand = c(0,0))+
  labs(#x=expression(paste("SW Anomaly [",sigma,"]")), 
       x=bquote(paste('Short Wave anomaly'["3-mo"]," [",sigma,"]")),
       y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')))+
  annotate("text",label="Darker",x=-1.3,y=-0.1,size=6, fontface='bold')+
  annotate("text",label="Brighter",x=1.3,y=-0.1, size=6, fontface='bold')+theme(legend.position = 'none')
p1c

p1d <- 
  tibble(u_VPDmean.tc=seq(0.35,1.75,length.out=500), 
         u_Tmean.tc=mean(stem_dry$u_Tmean.tc),
         u_sw.tc=mean(stem_dry$u_sw.tc),
         sw_anom_sigma_3mo.tc = 0,
         dry_anom_sigma.tc=0,
         wet_anom_sigma.tc=0) %>% 
  add_predicted_draws(model = s_d4_preds, newdata = ., n = 3000, re_formula = NA) %>% 
  ggplot(data=., aes(u_VPDmean.tc, NA))+
  stat_lineribbon(aes(y = .prediction-0.2), .width = c(.99, .75, .50), alpha = 0.45, col='black') +
  geom_point(data=stem_dry, aes(u_VPDmean.tc, stem),
              alpha=0.4,cex=2, col="#352702")+
  scale_fill_viridis_d("Probability",option = "C", direction = -1, begin = 0, end = 0.95) + 
  scale_color_viridis_c()+
  scale_x_continuous(expand = c(0,0))+
  labs(x=bquote(paste('Monthly VPDmean'[mu],)), 
       y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')))+
  annotate("text",label="Humid",x=0.5,y=-0.1, size=6, fontface='bold')+
  annotate("text",label="Arid",x=1.5,y=-0.1, size=6, fontface='bold')+ theme(legend.position = 'none')
p1d


# wet forest coef plot
plot(s_wcombo, pars=names(s_wcombo$coefficients)[2:4])
p2 <- plot(s_wcombo, pars=names(s_wcombo$coefficients)[2:4]); p2
p2a <- p2+scale_y_discrete(limits=rev(c("VPDmean_anom_sigma_3mo.tc", 
                                       "def_anom_sigma.tc",
                                       "sw_anom_sigma_3mo.ceres")),
                          labels=rev(c(bquote(paste('VPD anom.'["3-mo"])),
                                       bquote(paste('Water deficit anom.')),
                                       bquote(paste('SW anom.'["3-mo"])))))+
  labs(
    # title=bquote(paste("Aseasonal Wet Forest NPP"["stem"]," Model")), 
       x="Standardized Effect Size")+
  ggtitle(label = bquote(paste("Aseasonal Wet Forest Model")))+
  theme(axis.text=element_text(size=14, face='bold'), 
        axis.title = element_text(size=16, face='bold'),
        plot.title = element_text(size=22,face='bold'))
p2a; 

p2b <-
  tibble(VPDmean_anom_sigma_3mo.tc=0, 
         def_anom_sigma.tc=seq(-1.5,3.5,length.out=500),
         sw_anom_sigma_3mo.ceres=0) %>% 
  add_predicted_draws(model = s_wcombo, newdata = ., n = 3000, re_formula = NA) %>% 
  ggplot(data=., aes(def_anom_sigma.tc, NA))+
  geom_vline(xintercept = 0, col='grey')+
  stat_lineribbon(aes(y = .prediction-0.2), .width = c(.99, .75, .50), alpha = 0.45, col='#353533') +
  geom_jitter(data=stem_wet, aes(def_anom_sigma.tc, stem),
             alpha=0.5,cex=3, col="#352702", width = 0.5)+
  scale_fill_viridis_d("Probability",option = "B", direction = -1, begin = 0.1, end = 0.9) + 
  scale_color_viridis_c()+
  scale_x_continuous(expand = c(0,0.1))+
  labs(x=expression(paste("Water Deficit Anomaly [",sigma,"]")), 
       y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1'))) +
  annotate("text",label="Wetter",x=-0.5,y=-0.1,size=6, fontface='bold')+
  annotate("text",label="Drier",x=2,y=-0.1, size=6, fontface='bold')+ theme(legend.position = 'none')
p2b

p2c <- 
  tibble(VPDmean_anom_sigma_3mo.tc=0, 
         def_anom_sigma.tc=0,
         sw_anom_sigma_3mo.ceres=seq(-1.5,1.5,length.out=500)) %>% 
  add_predicted_draws(model = s_wcombo, newdata = ., n = 3000, re_formula = NA) %>% 
  ggplot(data=., aes(sw_anom_sigma_3mo.ceres, NA))+
  geom_vline(xintercept = 0, col='grey')+
  stat_lineribbon(aes(y = .prediction-0.2), .width = c(.99, .75, .50), alpha = 0.4, col='black') +
  geom_point(data=stem_wet, aes(sw_anom_sigma_3mo.ceres, stem),
             alpha=0.4,cex=2, col="#352702")+
  scale_fill_viridis_d("Probability",option = "D", direction = -1, begin = 0, end = 0.95) + 
  scale_color_viridis_c()+
  scale_x_continuous(expand = c(0,0))+
  labs(x=bquote(paste('Short Wave anomaly'["3-mo"]," [",sigma,"]")), 
       y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')))+
  annotate("text",label="Darker",x=-0.75,y=-0.1, size=5, fontface='bold')+
  annotate("text",label="Brighter",x=0.75,y=-0.1, size=5, fontface='bold')+ theme(legend.position = 'none')
p2c

p2d <- 
  tibble(VPDmean_anom_sigma_3mo.tc=seq(-1,3,length.out=500), 
         def_anom_sigma.tc=0,
         sw_anom_sigma_3mo.ceres=0) %>% 
  add_predicted_draws(model = s_wcombo, newdata = ., n = 3000, re_formula = NA) %>% 
  ggplot(data=., aes(VPDmean_anom_sigma_3mo.tc, NA))+
  geom_vline(xintercept = 0, col='grey')+
  stat_lineribbon(aes(y = .prediction-0.2), .width = c(.99, .75, .50), alpha = 0.45, col='black') +
  geom_point(data=stem_wet, aes(VPDmean_anom_sigma_3mo.tc, stem),
             alpha=0.4,cex=2, col="#352702")+
  scale_fill_viridis_d("Probability",option = "C", direction = -1, begin = 0.1, end = 0.9) + 
  scale_color_viridis_c()+
  scale_x_continuous(expand = c(0,0))+
  labs(
    x=bquote(paste('VPDmean anomaly'["3-mo"]," [",sigma,"]")),
    y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')))+
  annotate("text",label="Humid",x=-0.5,y=-0.1, size=6, fontface='bold')+
  annotate("text",label="Arid",x=2.25,y=-0.1, size=6, fontface='bold')+ theme(legend.position = 'none')
p2d

library(cowplot)
p_top <- plot_grid(p1a,p2a,nrow = 1, labels=c("A","B"))
p_high <- plot_grid(p1b, p2b, nrow=1, labels=c("C","D"))
p_mid <- plot_grid(p1c, p2c, nrow=1, labels=c("E","F"))
p_low <- plot_grid(p1d, p2d, nrow=1, labels=c("G","H"))
p_joined <- plot_grid(p_top,p_high,p_mid,p_low, ncol=1, rel_heights = c(0.8,1,1,1))
p_joined <- p_joined + geom_rect(aes(xmin=0.501,xmax=1,ymin=0.005,ymax=0.9975),
                                 color=NA,fill="blue",alpha=0.025)+
           geom_rect(aes(xmin=0.0,xmax=0.499,ymin=0.005,ymax=0.9975),
                     color=NA,fill="orange",alpha=0.025)

save_plot(filename = paste("figures/EffectsAndCondPreds_StemNPP_",Sys.Date(),".png"),
          plot=p_joined,
          base_height=14, base_width = 12)
#****END*****************************************************************************


#*************************************************************************************************
# Figure 2 --- Plot site level predictions -----------------------------------------
pal_wet <- viridis(n = 5, begin = 0.1, end = 0.9, option = "D"); 
pal_dry <- viridis(n = 5, begin = 0, end = 0.8, option = "B"); 

clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
        is.na(cloud_fraction_anom_12moAvg)==F & 
                                      is.na(cloud_fraction_anom_sigma_3mo)==F & 
                                      is.na(sw_anom_sigma_3mo.ceres)==F) %>% 
  sapply(., function(x) sum(is.na(x))) %>% sum

tmp_dat <- stem_tc %>% filter(plot_code=="KEN-01")
p4a <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>% 
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_d4_preds, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_d4_preds, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_dry[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_dry[4])+
  geom_line(color=pal_dry[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="KEN-01, Kenia, Bolivia");

tmp_dat <- stem_tc %>% filter(plot_code=="TAM-05")
p4b <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>%  
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_d4_preds, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_d4_preds, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_dry[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_dry[4])+
  geom_line(color=pal_dry[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="TAM-05, Tambopata, Perú");

tmp_dat <- stem_tc %>% filter(plot_code=="STO-07")
p4c <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>% 
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_d4_preds, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_d4_preds, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_dry[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_dry[4])+
  geom_line(color=pal_dry[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="STO-07, Santarém, Brazil");

tmp_dat <- stem_tc %>% filter(plot_code=="KOG-04")
p4d <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>% 
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_d4_preds, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_d4_preds, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_dry[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_dry[4])+
  geom_line(color=pal_dry[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="KOG-04, Kogyae, Ghana")+
  scale_x_datetime(date_labels = "%Y-%m");

tmp_dat <- stem_tc %>% filter(plot_code=="BOB-01")
p4e <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>% 
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_d4_preds, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_d4_preds, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_d4_preds,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_dry[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_dry[4])+
  geom_line(color=pal_dry[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="BOB-01, Bobiri, Ghana");


tmp_dat <- stem_tc %>% filter(plot_code=="SAF-02")
p4f <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>%  
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_wcombo, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_wcombo, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_wet[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_wet[4])+
  geom_line(color=pal_wet[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="SAF-02, Sabah, Malaysian Borneo");

tmp_dat <- stem_tc %>% filter(plot_code=="MLA-01")
p4g <-clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>%  
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_wcombo, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_wcombo, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_wet[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_wet[4])+
  geom_line(color=pal_wet[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="MLA-01, Sabah, Malaysian Borneo");

tmp_dat <- stem_tc %>% filter(plot_code=="JEN-11")
p4h <- clim %>% 
  select(-SWmean, -VPDmean_anom_sigma_3mo, -PARmean, -Tmean_anom_sigma_3mo, -cwd_rat) %>% 
  filter(year>2004) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F &
           is.na(cloud_fraction_anom_12moAvg)==F & 
           is.na(cloud_fraction_anom_sigma_3mo)==F & 
           is.na(sw_anom_sigma_3mo.ceres)==F) %>% 
  filter(site==unique(tmp_dat$site)) %>% mutate(plot_code=unique(tmp_dat$plot_code)) %>% 
  filter(is.na(VPDmean_anom_12moAvg)==F) %>% 
  filter(date >= (min(tmp_dat$date))-months(1) & 
           date <= max(tmp_dat$date)+months(1)) %>% 
  mutate(pred_y_50 = posterior_predict(s_wcombo, newdata = .) %>% apply(., 2, quantile, 0.5)-0.2) %>% 
  mutate(pred_y_90 = posterior_predict(s_wcombo, newdata=.) %>% apply(., 2, quantile, 0.9)-0.2) %>% 
  mutate(pred_y_75 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.75)-0.2) %>% 
  mutate(pred_y_25 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.25)-0.2) %>% 
  mutate(pred_y_10 = posterior_predict(s_wcombo,newdata=.) %>% apply(., 2, quantile, 0.10)-0.2) %>% 
  ggplot(data=., aes(date, pred_y_50))+#geom_point()+
  geom_ribbon(aes(date, ymin=pred_y_10, ymax=pred_y_90),lty=0,alpha=0.25, fill=pal_wet[5])+
  geom_ribbon(aes(date, ymin=pred_y_25, ymax=pred_y_75),lty=0,alpha=0.25, fill=pal_wet[4])+
  geom_line(color=pal_wet[1])+
  geom_point(data=tmp_dat %>% filter(plot_code==unique(tmp_dat$plot_code)), aes(date, stem),pch=20, cex=3, alpha=0.7)+
  geom_hline(aes(yintercept=0),col="gray50",lty=3)+
  labs(x=NULL,y=bquote(paste('NPP'['stem'],' MgC ha'^'-1','mo'^'-1')),title="JEN-11, Jenaro Herrera, Perú");

library(cowplot)
row_3 <- plot_grid(p4c,p4d,p4e,p4f,p4g,p4h, labels=c("C","D","E","F","G","H"), ncol = 2, label_x=0.05)
p4_joined <- plot_grid(p4a,p4b,row_3, labels=c("A","B",NULL), label_x= c(0.05,0.05,NA),
                       ncol = 1, rel_heights = c(0.75,0.75,2.5))
p4_joined
save_plot(filename = paste("figures/SitePreds_StemNPP_timeSeries_",Sys.Date(),".png"),
          plot=p4_joined, ncol=2,  
          base_height=11, base_width = 6)
#************************************************************************************************
# End plot site level predictions
#************************************************************************************************


