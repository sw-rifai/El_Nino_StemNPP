library(tidync); library(tidyverse); library(lubridate); library(RcppRoll)
library(egg); library(grid); library(gridExtra); library(gtable)
library(gganimate)
#--- NEEDED IMPORTS ---
gfc <- read_csv("../data_general/GFC_forestArea/gfc_forestArea_CRU_grid.csv")
gfc <- gfc %>% mutate(lat=ymax-0.25, lon=xmax-0.25) %>% 
  mutate(forestAreaHa=(sum/1e5), 
         forestAreaKm2=0.01*(sum/(1000**2))) 
gfc %>% ggplot(data=., aes(lon,lat))+geom_raster(aes(fill=forestAreaKm2))+coord_equal()+
  scale_fill_viridis_c()
gfc <- gfc %>% select(lat,lon,forestAreaKm2)
#----------------------

#--- El Niño 3.4 Index ------------------
en34 <- read_csv("../data_general/SST/nino34_1870_2017.csv")

# Multivariate El Nino index ----------------------------------------------
mei <- read_csv("../data_general/SST/mei_1995_2016.csv")

# Import Stem NPP predictions ---------------------------------------------
stem_grid <- tidync("outputs/grid_preds/stem_bayesGLMM_wetDry_terraClim_chirps_199101_201612.nc") %>% hyper_tibble()
stem_grid <- stem_grid %>% mutate(date=as.POSIXct("1991-01-01",tz='UTC')+months(time))
stem_grid <- left_join(stem_grid %>% rename(lon=longitude, lat=latitude, stem=variable), gfc, by=c("lon","lat"))
stem_grid <- stem_grid %>% mutate(stem_Pg=stem*forestAreaKm2*100*1e-9) 


# Stem anomalies ----------------------------------------------------------
stem_anoms <- tidync("outputs/grid_preds/anoms/stem_detrend_anoms_bayesGLMM_wetDry_terraClim_chirps_199101_201612.nc") %>% hyper_tibble()
anoms <- left_join(stem_anoms %>% rename(lon=longitude, lat=latitude,stem=variable), gfc)
anoms <- anoms %>% mutate(date=as.POSIXct("1991-01-01",tz='UTC')+months(time))
anoms <- anoms %>% mutate(stem_Pg=stem*forestAreaKm2*100*1e-9)


# NPP stem summary statistics ---------------------------------------------
stem_grid %>% 
  mutate(year=year(date)) %>% filter(year>=1991 & year<=2016) %>% 
  group_by(date) %>% summarize(tot = sum(stem_Pg)) %>% ungroup() %>% 
  mutate(tot_12mo = roll_sumr(tot, n=12)) %>% 
  ggplot(data=., aes(date, tot_12mo))+geom_line()

stem_grid %>% 
  mutate(year=year(date)) %>% filter(year>=1996 & year<=2016) %>% 
  # filter(year <= 2005) %>% 
  group_by(date) %>% summarize(tot = sum(stem_Pg)) %>% ungroup() %>% 
  mutate(tot_12mo = roll_sumr(tot, n=12)) %>% pull(tot_12mo) %>% summary
  
1 - 1.962/2.155
1 - 1.976/2.155

stem_grid %>% 
  mutate(year=year(date)) %>% 
  filter(year>=1995 & year<=2016) %>%
  group_by(year) %>% summarize(tot = sum(stem_Pg)) %>% pull(tot) %>% summary
  
stem_grid %>% 
  mutate(year=year(date)) %>% 
  filter(year>=1991 & year<=2016) %>% 
  group_by(year) %>% summarize(tot = sum(stem_Pg)) %>% 
  lm(tot~year, data=.) %>% summary
  ggplot(data=., aes(year, tot))+geom_point()+geom_smooth(method='lm')
stem_grid

tmp <- stem_grid %>% 
  # mutate(region=cut(lon, breaks=c(-Inf,-25,55,Inf), labels=c("Americas","Africa","AsiaPacific"))) %>% 
  group_by(date) %>% 
  summarize(tot_stem=sum(stem_Pg)) %>% ungroup() %>% 
  # filter(region=="Americas") %>% 
  mutate(u_12mo = roll_sumr(tot_stem, n=12, na.rm=T)) %>% 
  left_join(., en34 %>% select( date,index), by='date') %>% 
  mutate(index_12mo = roll_mean(index, n=12, na.rm=T, fill = NA)) %>% 
  filter(date>=as.POSIXct("1992-01-01",tz="UTC")) %>% 
  mutate(month=month(date), year=year(date))
tmp_fit <- gam(u_12mo~s(index_12mo)+year, data=tmp, method='ML')
tmp_fit <- gam(tot_stem~s(index)+s(month, bs='cc')+year,  method='ML', 
               data=tmp)
stmp_fit <- stan_gamm4(tot_stem~s(index)+s(month,bs='cc')+year, data=tmp)
bayes_R2(stmp_fit) %>% median()
plot_nonlinear(stmp_fit)
plot(stmp_fit, pars='year')

tmp_2 <- tmp %>% group_by(year) %>% 
             summarize(u_12mo=mean(u_12mo),
            index=mean(index), 
            index_12mo = mean(index_12mo)) %>% ungroup()
stmp_fit2 <- stan_glm(u_12mo~index_12mo+year, 
                      data=tmp_2, 
                      prior = normal(0,1),QR=T, 
                      algorithm = "sampling")
summary(stmp_fit2, digits=3)
pp_check(stmp_fit2)
bayes_R2(stmp_fit2) %>% median()
plot(stmp_fit2, pars=c("index_12mo","year"))

tmp_2 %>% filter(is.na(index_12mo)==F) %>% 
  add_predicted_draws(stmp_fit2) %>% 
  ggplot(data=., aes(year, u_12mo)) +
  stat_lineribbon(aes(y = .prediction), .width = c(.9, .75, .50), alpha = 0.25, col='black') +
  labs(x='Year',y=bquote(paste('NPP'['stem'],' [Pg C yr'^-1,']')))+
  geom_point()

stmp_fit3 <- stan_gamm4(u_12mo~s(index_12mo)+s(year), 
                      data=tmp_2, 
                      prior = normal(0,1),#QR=T, 
                      algorithm = "sampling")
plot_nonlinear(stmp_fit3)
tmp_2 %>% filter(is.na(index_12mo)==F) %>% 
  add_predicted_draws(stmp_fit3) %>% 
  ggplot(data=., aes(year, u_12mo)) +
  stat_lineribbon(aes(y = .prediction), .width = c(.9, .75, .50), alpha = 0.25, col='black') +
  labs(x='Year',y=bquote(paste('NPP'['stem'],' [Pg C yr'^-1,']')))+
  geom_point()


tmp_2 %>% 
  ggplot(data=., aes(year, u_12mo))+
  geom_point(col='darkgreen',size=5)+
  geom_smooth(method='lm',se=F,col='black')+
  geom_smooth(data=(tmp_2 %>% filter(year != 1998 & year != 2016)), col='darkgrey',lty=2, method='lm', se=F)+
  labs(x='Year',y=bquote(paste('NPP'['stem'],' [Pg C yr'^-1,']')))+
  scale_x_continuous(breaks = seq(1992,2016,by = 2))
ggsave(filename = paste0("figures/PantropicalNPPstemPgYr_1992_2016_produced",Sys.Date(),".png"))

visreg(tmp_fit)
visreg(tmp_fit, xvar='month', scale='response')
visreg(tmp_fit, xvar='index', scale='response')
visreg(tmp_fit, xvar='year', scale='response')

tmp %>% filter(year>1998 & year<2015) %>% 
  ggplot(data=., aes(date,u_12mo,color=index_12mo))+geom_point()+
  geom_smooth(method='lm')+
  scale_color_viridis_c()


# Stem anomaly with climate vars ------------------------------------------
tmp <- readRDS("../data_general/clim_grid/TerraClimate_CHIRPS_gridDF_1990_2017.rds")
tmp <- tmp %>% filter(date==tmp$date[1]) %>% select(lon,lat,S, map)
clim_grid <- readRDS("../data_general/clim_grid/TerraClimate_StemNPP_predGridDF_1990_2017.rds")
clim_grid <- left_join(clim_grid, tmp, by=c("lon","lat"))
en34 <- read_csv("../data_general/SST/nino34_1870_2017.csv")

tmp2 <- clim_grid %>% group_by(date) %>% summarize(vpd_sigma=mean(VPDmean_anom_sigma.tc, na.rm=T), 
                                                   tmean_sigma=mean(Tmean_anom_sigma.tc, na.rm=T), 
                                                   def_sigma=mean(def_anom_sigma.tc, na.rm=T),
                                                   sw_sigma.tc=mean(sw_anom_sigma.tc, na.rm=T),
                                                   sw_sigma.ceres=mean(sw_anom_sigma.ceres, na.rm=T),
                                                   vpd=mean(VPDmean_anom.tc, na.rm=T), 
                                                   tmean=mean(Tmean_anom.tc, na.rm=T), 
                                                   def=mean(def_anom.tc, na.rm=T), 
                                                   sw.tc=mean(sw_anom.tc, na.rm=T), 
                                                   sw.ceres=mean(sw_anom.ceres, na.rm=T))
tmp2 <- tmp2 %>% #left_join(., en34, by='date') %>%
  mutate(vpd_sigma_12 = roll_meanr(vpd_sigma, n=12, fill=NA), 
         tmean_sigma_12 = roll_meanr(tmean_sigma, n=12, fill=NA), 
         def_sigma_12 = roll_meanr(def_sigma,n=12,fill=NA), 
         sw_sigma_12.tc = roll_meanr(sw_sigma.tc, n=12, fill=NA), 
         sw_sigma_12.ceres = roll_meanr(sw_sigma.ceres, n=12, fill=NA), 
         vpd_12 = roll_meanr(vpd,n=12,fill=NA), 
         tmean_12 = roll_meanr(tmean,n=12,fill=NA), 
         def_12=roll_meanr(def,n=12,fill=NA), 
         sw_12.tc=roll_meanr(sw.tc,n=12,fill=NA), 
         sw_12.ceres=roll_meanr(sw.ceres,n=12,fill=NA))

tmp <- anoms %>% 
  # mutate(region=cut(lon, breaks=c(-Inf,-25,55,Inf), labels=c("Americas","Africa","AsiaPacific"))) %>% 
  group_by(date) %>% 
  summarize(tot_stem=sum(stem_Pg)) %>% ungroup() %>% 
  # filter(region=="Americas") %>% 
  mutate(u_12mo = roll_sumr(tot_stem, n=12, na.rm=T)) %>% 
  left_join(., en34 %>% select( date,index), by='date') %>%
  mutate(index_12mo = roll_meanr(index, n=12, na.rm=T, fill = NA)) %>%
  filter(date>=as.POSIXct("1992-01-01",tz="UTC")) %>% 
  mutate(month=month(date), year=year(date))

left_join(tmp, tmp2, by="date") %>% select(u_12mo, index_12mo, vpd_sigma_12, tmean_sigma_12,
                                           sw_sigma_12.tc, def_sigma_12) %>% cor




# Calculate Stem El Nino correlation --------------------------------------
tmp <- anoms %>% 
  # mutate(region=cut(lon, breaks=c(-Inf,-25,55,Inf), labels=c("Americas","Africa","AsiaPacific"))) %>% 
  group_by(date) %>% 
  summarize(tot_stem=sum(stem_Pg, na.rm=T)) %>% ungroup() %>% 
  # filter(region=="Americas") %>% 
  mutate(u_12mo = roll_sumr(tot_stem, n=12, na.rm=T, fill=NA)) %>% 
  left_join(., en34 %>% select( date,index), by='date') %>%
  mutate(index_12mo = roll_mean(index, n=12, na.rm=T, fill = NA)) %>% 
  filter(date>=as.POSIXct("1992-01-01",tz="UTC")) %>% 
  filter(is.na(u_12mo)==F & is.na(index_12mo)==F)
tmp %>% select(-date) %>% cor
tmp %>% filter(is.na(u_12mo)==F) %>% select(index_12mo, u_12mo) %>% cor
tmp %>% mutate(pct = u_12mo*100/2.171) %>% View
  min(100*tmp$u_12mo/2.171)
tmp %>% ggplot(data=., aes(date, 100*u_12mo/2.171))+geom_line()+theme_bw()

tmp <- stem_grid %>% 
  # mutate(region=cut(lon, breaks=c(-Inf,-25,55,Inf), labels=c("Americas","Africa","AsiaPacific"))) %>% 
  group_by(date) %>% 
  summarize(tot_stem=sum(stem_Pg, na.rm=T)) %>% ungroup() %>% 
  # filter(region=="Americas") %>% 
  mutate(u_12mo = roll_sumr(tot_stem, n=12, na.rm=T, fill=NA)) %>% 
  left_join(., en34 %>% select( date,index), by='date') %>%
  mutate(index_12mo = roll_mean(index, n=12, na.rm=T, fill = NA)) %>% 
  filter(date>=as.POSIXct("1992-01-01",tz="UTC")) %>% 
  filter(is.na(u_12mo)==F & is.na(index_12mo)==F)
tmp %>% select(-date) %>% cor
tmp %>% filter(is.na(u_12mo)==F) %>% select(index_12mo, u_12mo) %>% cor



stem_grid %>% 
  filter(forestAreaKm2 > 250) %>% 
  mutate(epoch = cut(date, breaks = c(
    as.POSIXct("1997-08-01",tz="UTC"),                                       
    as.POSIXct("1998-08-01",tz="UTC"), 
    as.POSIXct("2001-08-01",tz="UTC"), 
    # as.POSIXct("2010-08-01",tz="UTC"), 
    as.POSIXct("2015-08-01",tz="UTC"), 
    as.POSIXct("2016-08-01",tz="UTC")), 
    labels=c('El Niño 1997-1998',
             'junk1',
             '2001-2015 Reference period',
             # 'junk2',
             'El Niño 2015-2016'))) %>% 
  # filter(date >= as.POSIXct("2015-08-01",tz="UTC") & date <= as.POSIXct("2016-08-01",tz="UTC")) %>% 
  group_by(epoch, lon,lat) %>% 
  summarize(tot=mean(stem_Pg, na.rm=T)*12) %>% #ungroup() %>% pull(tot) %>% quantile(., c(0.001,0.999))
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  filter(epoch=='2001-2015 Reference period') %>% 
  ggplot(data=., aes(lon,lat))+geom_raster(aes(fill=tot))+coord_equal()+
  # scale_fill_gradient2("Stem NPP\nAnomaly [PgC/yr]",low = "white",
  #                      mid = "yellow",
  #                      high = "darkgreen",
  #                      # midpoint = 0,
  #                      # limits=quantile(tot, 0.05,0.95),
  #                      # limits=c(-5e-04,5e-04 ),
  #                      # space = "Lab",
  #                      na.value = "grey50",
  #                      guide = "colourbar")+
  scale_fill_viridis_c("Stem NPP\n[PgC/yr]", option = "D", oob=scales::squish, limits=c(0,0.0009))+
  labs(x="Longitude",y="Latitude",title="Forest Woody Stem NPP")+
  theme_dark()+#+ggtitle("Stem Respiration Anomaly")
  # theme(legend.position="bottom")+
  facet_wrap(~epoch, nrow=3)
ggsave(filename = paste("figures/Map_StemNPP_2001_2015_RefPeriod_",Sys.Date(),".png"))


p1 <- stem %>% mutate(month=month(date)) %>% 
  filter(forestAreaKm2 > 250) %>%
  filter(date >=     as.POSIXct("2001-08-01",tz="UTC")) %>% 
  filter(date <=     as.POSIXct("2015-08-01",tz="UTC")) %>% 
  group_by(month, lon,lat) %>% 
  summarize(tot=mean(stem_Pg, na.rm=T)) %>% #ungroup() %>% pull(tot) %>% quantile(., c(0.001,0.999))
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  # filter(epoch=='2001-2015 Reference period') %>% 
  ggplot(data=., aes(lon,lat,frame=month))+geom_raster(aes(fill=tot))+coord_equal()+
  # scale_fill_gradient2("Stem NPP\nAnomaly [PgC/yr]",low = "white",
  #                      mid = "yellow",
  #                      high = "darkgreen",
  #                      # midpoint = 0,
  #                      # limits=quantile(tot, 0.05,0.95),
  #                      # limits=c(-5e-04,5e-04 ),
  #                      # space = "Lab",
  #                      na.value = "grey50",
  #                      guide = "colourbar")+
  scale_fill_viridis_c("Stem NPP\n[PgC/yr]", option = "D")+
  labs(x="Longitude",y="Latitude",title="Forest Woody Stem NPP")+
  theme_dark()+ggtitle("Forest Woody Stem NPP   Month:") # +
# theme(legend.position="bottom")+
# facet_wrap(~month)
gganimate(p1, paste0("figures/StemNPP_byMonth_2001_2015_RefPeriod_",Sys.Date(),"_.gif"), 
          ani.height = 1200, 
          ani.width = 1800)


stem %>% filter(date>as.POSIXct("1990-12-01",tz="UTC")) %>% 
  filter(date<as.POSIXct("2017-01-01",tz="UTC")) %>% 
  group_by(date) %>% 
  summarize(stem=sum(stem_Pg,na.rm=T)) %>% 
  mutate(u_12mo = roll_sumr(stem, 12)) %>% pull(u_12mo) %>% summary


# El Nino StemNPP anomaly using PgC ------------------------------------------------------------------
anoms %>% 
  filter(forestAreaKm2 > 250) %>% 
  mutate(epoch = cut(date, breaks = c(
    as.POSIXct("1997-08-01",tz="UTC"),                                       
    as.POSIXct("1998-08-01",tz="UTC"), 
    as.POSIXct("2001-08-01",tz="UTC"), 
    # as.POSIXct("2010-08-01",tz="UTC"), 
    as.POSIXct("2015-08-01",tz="UTC"), 
    as.POSIXct("2016-08-01",tz="UTC")), 
    labels=c('El Niño 1997-1998',
             'junk1',
             '2001-2015 Reference period',
             # 'junk2',
             'El Niño 2015-2016'))) %>% 
  # filter(date >= as.POSIXct("2015-08-01",tz="UTC") & date <= as.POSIXct("2016-08-01",tz="UTC")) %>% 
  group_by(epoch, lon,lat) %>% 
  summarize(tot=mean(stem_Pg, na.rm=T)*12) %>% #ungroup() %>% pull(tot) %>% quantile(., c(0.001,0.999))
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  filter(epoch=='El Niño 1997-1998' | epoch=='El Niño 2015-2016') %>% 
  ggplot(data=., aes(lon,lat))+geom_raster(aes(fill=tot))+coord_equal()+
  # scale_fill_gradient2("Stem NPP\nAnomaly [PgC/yr]",
  #                      low = "#40004b",
  #                      mid = "#fffff7",
  #                      high = "#00441b",
  #                      # midpoint = 0,
  #                      # limits=quantile(tot, 0.05,0.95),
  #                      limits=c(-2.5e-04,2.5e-04 ),
  #                      # space = "Lab",
  #                      na.value = "grey50",
  #                      guide = "colourbar", 
  #                      oob=scales::squish)+
  scale_fill_gradientn("Stem NPP\nAnomaly [PgC/yr]",
    #                      low = "#40004b",
                         # mid = "#fffff7",
    #                      high = "#00441b",
    #                      # midpoint = 0,
    #                      # limits=quantile(tot, 0.05,0.95),
                         limits=c(-2.25e-04,2.25e-04 ),
    #                      # space = "Lab",
                         na.value = "grey50",
    #                      guide = "colourbar", 
                         oob=scales::squish, 
    colors = RColorBrewer::brewer.pal(name="PRGn",10))+
  labs(x="Longitude",y="Latitude",
       title=bquote(paste('Predicted Tropical Forest NPP'["Stem"],' Anomaly'))) +
  theme_dark()+#+ggtitle("Stem Respiration Anomaly")
  # theme(legend.position="bottom")+
  facet_wrap(~epoch, nrow=2)
ggsave(filename = paste("figures/Map_StemNPP_El_Nino_Anomalies_",Sys.Date(),".png"))

# El Nino StemNPP anomaly using MgC/(ha*yr) --------------------------------------------------------------
anoms %>% 
  filter(forestAreaKm2 > 500) %>% 
  mutate(epoch = cut(date, breaks = c(
    as.POSIXct("1997-06-15",tz="UTC"),                                       
    as.POSIXct("1998-07-15",tz="UTC"), 
    as.POSIXct("2001-07-15",tz="UTC"), 
    # as.POSIXct("2010-08-01",tz="UTC"), 
    as.POSIXct("2015-06-15",tz="UTC"), 
    as.POSIXct("2016-07-15",tz="UTC")), 
    labels=c('El Niño 1997-1998',
             'junk1',
             '2001-2015 Reference period',
             # 'junk2',
             'El Niño 2015-2016'))) %>% 
  # filter(date >= as.POSIXct("2015-08-01",tz="UTC") & date <= as.POSIXct("2016-08-01",tz="UTC")) %>% 
  group_by(epoch, lon,lat) %>% 
  summarize(tot=mean(stem, na.rm=T)*12) %>% #ungroup() %>% pull(tot) %>% quantile(., c(0.001,0.999))
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  filter(epoch=='El Niño 1997-1998' | epoch=='El Niño 2015-2016') %>% 
  ggplot(data=., aes(lon,lat))+geom_raster(aes(fill=tot))+coord_equal()+
  scale_fill_gradientn(
                         name=bquote(paste('MgC ha'^'-1','yr'^'-1')),
                       limits=c(-1,1),
                       na.value = "grey50",
                       oob=scales::squish, 
                       colors = RColorBrewer::brewer.pal(name="PRGn",10))+
  # scale_fill_gradient2(#"Stem NPP\nAnomaly [MgC/(ha*yr)]",
  #                      name=bquote(paste('MgC ha'^'-1','yr'^'-1')),
  #                      low = "red",
  #                      mid = "#fffff7",
  #                      high = "darkgreen",
  #                      midpoint = 0,
  #                      # limits=quantile(tot, 0.05,0.95),
  #                      limits=c(-1,1),
  #                      # space = "Lab",
  #                      na.value = "grey50",
  #                      guide = "colourbar", 
  #                      oob=scales::squish)+
  # scale_fill_viridis_c("Stem NPP\n[PgC/yr]", option = "D")+
  labs(x="Longitude",y="Latitude",
       title=bquote(paste('Predicted Tropical Forest NPP'["Stem"],' Anomaly'))) +
  theme_dark()+#+ggtitle("Stem Respiration Anomaly")
  # theme(legend.position="bottom")+
  facet_wrap(~epoch, nrow=2)
ggsave(filename = paste("figures/Map_StemNPP_El_Nino_Anomalies_MgChaYr",Sys.Date(),".png"))

anoms %>% 
  # left_join(., gfc %>% select(lon,lat,forestAreaKm2), by=c("lon","lat")) %>%
  mutate(region=cut(lon, breaks=c(-Inf,-25,55,Inf), labels=c("Americas","Africa","AsiaPacific"))) %>% 
  # filter(forestAreaKm2>750) %>%
  # group_by(date, region) %>%
  group_by(date) %>% 
  summarize(stem = sum(stem_Pg)) %>% 
  mutate(stem_12mo = roll_sumr(stem, n=12, na.rm=T)) %>% 
  left_join(., en34 %>% select(date,index), by='date') %>% 
  # filter(date>=as.POSIXct("1995-01-01",tz="UTC")) %>%
  # filter(date<as.POSIXct("2017-01-01",tz="UTC")) %>%
  ggplot(data=., aes(date, stem_12mo))+geom_line()+
  geom_rect(aes(NULL, NULL, xmin = date, xmax = date+months(1), fill = index),
            ymin = -100, ymax = 100)+
  geom_abline(aes(intercept=0,slope=0),col='grey')+
  geom_line(lwd=1)+
  geom_vline(aes(xintercept=as.POSIXct("1991-06-15",tz="UTC")))+
  scale_fill_viridis_c()+
  scale_x_datetime(date_breaks="2 years", date_labels = "%Y")+
  labs(y="NPP [Pg C/yr]",  x="",
       title="Stem NPP Anomaly - Tropical forest regions")+
  scale_fill_gradient2("El Niño 3.4",low = "blue",
                       mid = "white",
                       high = "red",
                       midpoint = 0,
                       # space = "Lab",
                       # na.value = "grey50",
                       guide = "colourbar")+
  theme_bw()+ylim(-0.35,0.35)+theme(legend.position='none')

# Forest Woody Stem NPP Anomaly -------------------------------------------
anoms %>% 
  mutate(region=cut(lon, breaks=c(-Inf,-25,55,Inf), labels=c("Americas","Africa","Asia-Pacific"))) %>% 
  bind_rows(., anoms %>% mutate(region="Pantropical")) %>% 
  group_by(date,region) %>% 
  summarize(stem = sum(stem_Pg)) %>% ungroup() %>% 
  group_by(region) %>% arrange(date) %>% 
  mutate(stem_12mo = roll_sumr(stem, n=12, na.rm=T)) %>% 
  left_join(., en34 %>% select(date,index), by='date') %>% 
  filter(date>=as.POSIXct("1996-01-01",tz="UTC")) %>%
  # filter(date<as.POSIXct("2017-01-01",tz="UTC")) %>%
  ggplot(data=., aes(date, stem_12mo))+geom_line()+
  geom_rect(aes(NULL, NULL, xmin = date, xmax = date+months(1), fill = index),
            ymin = -100, ymax = 100)+
  geom_abline(aes(intercept=0,slope=0),col='grey')+
  geom_line(lwd=1)+
  # scale_fill_viridis_c()+
  scale_x_datetime(date_breaks="2 years", date_labels = "%Y", 
                   expand = c(0,74161440*0.05),
                   # limits = c(as.POSIXct("1996-01-01",tz="UTC"),as.POSIXct("2017-01-01",tz="UTC"))
                   )+
  labs(y=bquote(paste('NPP'['stem'],' PgC yr'^'-1')),
       x="",
       title=bquote(paste('Tropical Forest NPP'['stem'], ' Anomaly')))+
  scale_fill_gradient2("El Niño 3.4 Index",low = "blue",
                       mid = "white",
                       high = "red",
                       midpoint = 0,
                       # space = "Lab",
                       # na.value = "grey50",
                       guide = "colourbar")+
  theme_bw()+
  # ylim(-0.2,0.2)+
  theme(legend.position="bottom", legend.direction = "horizontal",legend.box.spacing = unit(0,'cm'),
        legend.justification = c(0.35,0),
        strip.text = element_text(size=16), 
        legend.key.width = unit(2.75,'cm'))+
  facet_wrap(~region,nrow = 4, scales = "fixed")
ggsave(paste("figures/StemNPP_TimeSeries_RegionalDetrendStemAnomaly_",Sys.Date(),".png"), 
       height=8, width = 12)

# Forest Woody Stem NPP Anomaly -------------------------------------------
anoms %>% 
  group_by(date) %>% 
  summarize(stem = sum(stem_Pg)) %>% ungroup() %>% 
  mutate(stem_12mo = roll_sumr(stem, n=12, na.rm=T)) %>% 
  left_join(., en34 %>% select(date,index), by='date') %>% 
  filter(is.na(date)==F) %>% 
  filter(date>=as.POSIXct("1995-12-01",tz="UTC")) %>%
  ggplot(data=., aes(date, stem_12mo))+geom_line()+
  geom_rect(aes(NULL, NULL, xmin = date, xmax = date+months(1), fill = index),
            ymin = -100, ymax = 100, na.rm=T)+
  geom_abline(aes(intercept=0,slope=0),col='grey')+
  geom_line(lwd=1)+
  scale_fill_viridis_c()+
  labs(y=bquote(paste('NPP'['stem'],' PgC yr'^'-1')),
       x="",
       title=bquote(paste('Pantropical Forest NPP'['stem'], ' Anomaly')))+
  scale_fill_gradient2("El Niño 3.4 Index",
                       position='bottom',
                       low = "blue",
                       mid = "white",
                       high = "red",
                       midpoint = 0,
                       # space = "Lab",
                       # na.value = "grey50",
                       guide = "colourbar")+
  theme_bw()+
  scale_x_datetime(date_breaks="2 years", date_labels = "%Y", expand = c(0,0), 
                   limits = c(as.POSIXct("1995-12-01",tz="UTC"),as.POSIXct("2016-12-01",tz="UTC")))+
  theme(legend.direction = 'horizontal', legend.position = c(0.5,0.083))
ggsave(paste("figures/StemNPP_TimeSeries_PantropicalDetrendStemAnomaly_",Sys.Date(),".png"), 
       width = 10, height = 4.5)
       

       
       
# Monthly range of NPP stem ---------------------------------------------------------------------
stem_grid %>% 
  filter(forestAreaKm2 > 250) %>% 
  # mutate(epoch = cut(date, breaks = c(
  #   as.POSIXct("1997-08-01",tz="UTC"),                                       
  #   as.POSIXct("1998-08-01",tz="UTC"), 
  #   as.POSIXct("2001-08-01",tz="UTC"), 
  #   # as.POSIXct("2010-08-01",tz="UTC"), 
  #   as.POSIXct("2015-08-01",tz="UTC"), 
  #   as.POSIXct("2016-08-01",tz="UTC")), 
  #   labels=c('El Niño 1997-1998',
  #            'junk1',
  #            '2001-2015 Reference period',
  #            # 'junk2',
  #            'El Niño 2015-2016'))) %>% 
  filter(date >= as.POSIXct("1997-12-01",tz="UTC") & date <= as.POSIXct("2016-12-01",tz="UTC")) %>%
  mutate(month=month(date)) %>% 
  group_by(month, lon,lat) %>% 
  summarize(u=mean(stem, na.rm=T)) %>% #ungroup() %>% pull(tot) %>% quantile(., c(0.001,0.999))
  ungroup() %>% group_by(lon,lat) %>% 
  summarize(variance=diff(range(u, na.rm=T))) %>% 
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  ggplot(data=., aes(lon,lat))+geom_raster(aes(fill=variance))+coord_equal()+
  # scale_fill_gradient2("Stem NPP\nAnomaly [PgC/yr]",low = "white",
  #                      mid = "yellow",
  #                      high = "darkgreen",
  #                      # midpoint = 0,
  #                      # limits=quantile(tot, 0.05,0.95),
  #                      # limits=c(-5e-04,5e-04 ),
  #                      # space = "Lab",
  #                      na.value = "grey50",
  #                      guide = "colourbar")+
  scale_fill_viridis_c(bquote(paste(' MgC ha'^'-1','mo'^'-1')),
                       option = "A", 
                       # limits=c(0,0.005),
                       oob=scales::squish)+
  labs(x="Longitude",y="Latitude",title="Mean Seasonal Range of Tropical Forest Woody Stem NPP")+
  theme_dark()#+ggtitle("Stem Respiration Anomaly")
  # theme(legend.position="bottom")+
ggsave(filename = paste("figures/Map_StemNPP_Seasonal_Variation_",Sys.Date(),".png"))

# Monthly range of NPP stem ---------------------------------------------------------------------
stem %>% 
  filter(forestAreaKm2 > 250) %>% 
  # mutate(epoch = cut(date, breaks = c(
  #   as.POSIXct("1997-08-01",tz="UTC"),                                       
  #   as.POSIXct("1998-08-01",tz="UTC"), 
  #   as.POSIXct("2001-08-01",tz="UTC"), 
  #   # as.POSIXct("2010-08-01",tz="UTC"), 
  #   as.POSIXct("2015-08-01",tz="UTC"), 
  #   as.POSIXct("2016-08-01",tz="UTC")), 
  #   labels=c('El Niño 1997-1998',
  #            'junk1',
  #            '2001-2015 Reference period',
  #            # 'junk2',
#            'El Niño 2015-2016'))) %>% 
filter(date >= as.POSIXct("1997-12-01",tz="UTC") & date <= as.POSIXct("2016-12-01",tz="UTC")) %>%
  mutate(month=month(date)) %>% 
  group_by(month, lon,lat) %>% 
  summarize(u=mean(stem, na.rm=T)) %>% #ungroup() %>% pull(tot) %>% quantile(., c(0.001,0.999))
  ungroup() %>% group_by(lon,lat) %>% 
  summarize(variance=diff(range(u, na.rm=T))) %>% 
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  ggplot(data=., aes(lon,lat))+geom_raster(aes(fill=variance))+coord_equal()+
  # scale_fill_gradient2("Stem NPP\nAnomaly [PgC/yr]",low = "white",
  #                      mid = "yellow",
  #                      high = "darkgreen",
  #                      # midpoint = 0,
  #                      # limits=quantile(tot, 0.05,0.95),
  #                      # limits=c(-5e-04,5e-04 ),
  #                      # space = "Lab",
  #                      na.value = "grey50",
  #                      guide = "colourbar")+
  scale_fill_viridis_c(bquote(paste(' MgC ha'^'-1','mo'^'-1')),
                       option = "A", 
                       # limits=c(0,0.005),
                       oob=scales::squish)+
  labs(x="Longitude",y="Latitude",title="Mean Seasonal Range of Tropical Forest Woody Stem NPP")+
  theme_dark()#+ggtitle("Stem Respiration Anomaly")
# theme(legend.position="bottom")+
ggsave(filename = paste("figures/Map_StemNPP_Seasonal_Variation_",Sys.Date(),".png"))

out <- readRDS("../data_general/clim_grid/TerraClimate_CHIRPS_gridDF_1990_2017.rds")
out <- out %>% filter(date==out$date[1]) %>% select(lon,lat,S, map, mat, mav, ma_trange)
stem_grid %>% 
  filter(forestAreaKm2 > 50) %>% 
  filter(date >= as.POSIXct("1992-01-01",tz="UTC") & date <= as.POSIXct("2016-12-01",tz="UTC")) %>%
  mutate(month=month(date)) %>% 
  group_by(month, lon,lat) %>% 
  summarize(u=mean(stem)) %>% 
  ungroup() %>% left_join(., out) %>% 
  group_by(lon,lat) %>% 
  summarize(variance=diff(range(u, na.rm=T)), 
            S=mean(S, na.rm=T), 
            map=mean(map,na.rm=T), 
            mat=mean(mat.tc,na.rm=T),
            ma_trange=mean(ma_trange.tc, na.rm=T),
            mav=mean(mav,na.rm=T)) %>% 
  # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
  ggplot(data=., aes(S, variance,color=map, size=ma_trange))+geom_point(alpha=0.35)+
  scale_color_viridis_c("MAP [mm]", 
                        limits=c(1000,5000),oob=scales::squish, option = "C", direction = -1, end = 0.9)+
  labs(x='Precipitation Seasonality', y=bquote(paste('Intra-annual range of MgC ha'^'-1','mo'^'-1')), size="MAT range [C]")
ggsave(paste0("figures/NPPstemRange_Seasonality_MAT_scatter_",Sys.Date(),"_.png"),height = 5, width=7)

  stem %>% 
    filter(forestAreaKm2 > 250) %>% 
    filter(date >= as.POSIXct("1997-12-01",tz="UTC") & date <= as.POSIXct("2016-12-01",tz="UTC")) %>%
    mutate(month=month(date)) %>% 
    group_by(month, lon,lat) %>% 
    summarize(u=mean(stem)) %>% 
    ungroup() %>% left_join(., out) %>% 
    group_by(lon,lat) %>% 
    summarize(variance=diff(range(u, na.rm=T)), 
              S=mean(S, na.rm=T), 
              map=mean(map,na.rm=T), 
              mat=mean(mat,na.rm=T),
              ma_trange=mean(ma_trange, na.rm=T),
              mav=mean(mav,na.rm=T)) %>% 
    # filter(is.na(epoch)==F & epoch!='junk1' & epoch!='junk2') %>% 
    ggplot(data=., aes(S, variance))+
    geom_bin2d()+
    # geom_point(alpha=0.25)+
    scale_fill_viridis_c(
                          # limits=c(1000,5000),
                         oob=scales::squish, option = "C", direction = -1, end = 0.9)+
    labs(x='Precipitation Seasonality', y=bquote(paste('Intra-annual range of MgC ha'^'-1','mo'^'-1')))#+coord_equal()+
    # scale_fill_viridis_c(bquote(paste(' MgC ha'^'-1','mo'^'-1')),
    #                    option = "A", 
    #                    # limits=c(0,0.005),
    #                    oob=scales::squish)+
    # labs(x="Longitude",y="Latitude",title="Mean Seasonal Range of Tropical Forest Woody Stem NPP")+
    # theme_dark()
    # 

