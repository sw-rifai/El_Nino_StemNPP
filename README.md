# Data analysis for Rifai et al., 2019 Philosophical Transactions of the Royal Society B*

Note: Certain datasets used for scaling NPP across the tropics are too large to host in this repo and are referenced to in the code as a parent directory 'data_general'. These files can be downloaded via: https://drive.google.com/file/d/1F1T_w0nhW0j-Vy2kHtp9Jw2nyTT35VHp/view?usp=sharing

